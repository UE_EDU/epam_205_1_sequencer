# EPAM 205.1 Intro to Sequencer

Developed with Unreal Engine 5

## Task Description: 

This exercise will teach you how to create a basic Level Sequence with the Character Animation inside it. 

### Extra challenge: 

Please complete the base task as it was presented in the video first! In addition to that, you can experiment with different Character Animation from the Starter Pack and different combinations of Camera and Character movement in the Sequence. 

For example, a Character runs for a cover and ducks under it. Then he stands up and aims down the ironside. Multiple Camera Cuts are used to capture the action from different angles. 

### List of assets that should be present in the Project’s Content Folder: 

1. A map created from the Basic Template named ExampleMovieMap_P 
2. A Persistent Level named ExampleMovieMap_Cinematic 
3. Single Level Sequence named Scene_01 

### Requirements for the assets: 

1. Default Streaming Method for Persistent Level is set to Always Loaded 
2. Level Sequence is created for the Persistent Level 
3. Level Sequence have the following Tracks: 
	-- Camera Cut 
	-- CineCamera 
	-- Animation 

### Level Sequence requirements: 

All actors used in the Level Sequence are Possessable! 

1. Cine Camera actor moves on a curved path (not just a straight line!) 
2. Transform path can be done through regular Key points or by adjusting it via Sequencer’s Curve Editor. Either way is fine 
3. Character (UE4 Mannequin) moves forward and then to the right 
4. The following Character animation assets are used: Idle_Rifle_Ironsights, Walk_Fwd_Rifle_Ironsights, Walk_Rt_Rifle_Ironsights 
5. All three animations are on the same Animation track and the transition is blended between them 

![result.](/readme/result.gif "result.")

![master.](/readme/master.PNG "master.")

![scene_01.](/readme/scene_01.PNG "scene_01.")

![scene_02.](/readme/scene_02.PNG "scene_02.")